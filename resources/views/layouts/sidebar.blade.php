<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="#">Stisla</a>
    </div>
    <ul class="sidebar-menu">
      <li><a class="nav-link" href="/"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
    </ul>
    <ul class="sidebar-menu">
      <li><a class="nav-link" href="/posts"><i class="fas fa-newspaper"></i> <span>CRUD</span></a></li>
    </ul>
</div>
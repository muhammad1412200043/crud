@extends('layouts.app')

@section('title')
Show Post {{$post->id}}
@endsection
    
@section('content')
    <h4>{{$post->title}}</h4>
    <p>{{$post->body}}</p>
@endsection